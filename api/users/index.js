import express from 'express';
import User from './userModel';
import asyncHandler from 'express-async-handler';
import jwt from 'jsonwebtoken';
import movieModel from '../movies/movieModel';

const router = express.Router(); // eslint-disable-line

// Get all users
router.get('/', async (req, res) => {
    const users = await User.find();
    if(users.length > 0){
    res.status(200).json(users);
    }else{
      res.status(401).json({success: false, msg: "There are no users in the database."});
    }
});


// Register OR authenticate a user
router.post('/',asyncHandler( async (req, res, next) => {
    if (!req.body.username || !req.body.password) {
      res.status(401).json({success: false, msg: 'Please pass username and password.'});
      return next();
    }
    if (req.query.action === 'register') {
        var regEx = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/;
        var compare = regEx.test(req.body.password)
        if(compare){
            await User.create(req.body);
            res.status(201).json({code: 201, msg: 'Successful created new user.'});
        }else{
            res.status(401).json({success: false, msg: 'Please ensure password is at least 5 characters long and contains at least one number and one letter'});
        }

    } else {
      const user = await User.findByUserName(req.body.username);
        if (!user) return res.status(401).json({ code: 401, msg: 'Authentication failed. User not found.' });
        user.comparePassword(req.body.password, (err, isMatch) => {
          if (isMatch && !err) {
            // if user is found and password matches, create a token
            const token = jwt.sign(user.username, process.env.SECRET);
            // return the information including token as JSON
            res.status(200).json({success: true, token: 'BEARER ' + token});
          } else {
            res.status(401).json({code: 401,msg: 'Authentication failed. Wrong password.'});
          }
        });
      }
  }));

export default router;