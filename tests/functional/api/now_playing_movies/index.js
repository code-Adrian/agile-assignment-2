import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import api from "../../../../index";
import User from "../../../../api/users/userModel";

const expect = chai.expect;
let db;
let user1token;
let totalPages;

describe("Now Playing Movies", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
    try{
    await User.deleteMany();
    await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test1",
      });
      api.close();
    }catch(error){
        console.log(`Failed to create test user. Error: ${error}`)
    }
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  afterEach(() => {
    api.close(); // Release PORT 8080
  });


  describe("For receiving an authentication token", () => {
      it("should return a 200 status, a generated token and set global variable", () => {
        return request(api)
          .post("/api/users?action=authenticate")
          .send({
            username: "user1",
            password: "test1",
          })
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.token).to.not.be.undefined;
            user1token = res.body.token.substring(7);
          });
      });
  });

 
  describe("GET /api/movies/tmdb/now_playing/1 ", () => {
    it("should return status code of 401 (Unauthorized)", (done) => {
      request(api)
        .get("/api/movies/tmdb/now_playing/1")
        .expect(401)
        .end((err, res) => {
          expect(res.text).to.equal("Unauthorized")
          done();
        });
    });
  });

  describe("GET /api/movies/tmdb/now_playing/1 ", () => {
    it("should return 20 now_playing movies from page 1 and a status 200", (done) => {
      request(api)
        .get("/api/movies/tmdb/now_playing/1")
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          done();
        });
    });
  });

  describe("GET /api/movies/tmdb/now_playing/2 ", () => {
    it("should return 20 now_playing movies from page 2, set total pages variable expect a status code 200", (done) => {
      request(api)
        .get("/api/movies/tmdb/now_playing/2")
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          if(res.body.total_pages > 500){
            totalPages = 500
          }else{
            totalPages = res.body.total_pages
          }
          done();
        });
    });
  });

  describe("GET /api/movies/tmdb/now_playing/{last page} ", () => {
    it("should return more than 0 now_playing movies from last page and expect a status code 200", (done) => {
      request(api)
        .get(`/api/movies/tmdb/now_playing/${totalPages}`)
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.greaterThan(0);
          done();
        });
    });
  });

  describe("GET /api/movies/tmdb/now_playing/{invalid page} ", () => {
    it("should expect error 404 from requesting invalid page", (done) => {
      request(api)
        .get(`/api/movies/tmdb/now_playing/0`)
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(404)
        .end((err, res) => {
          expect(res.text).to.contain("Now playing movie page not found for page:");
          done();
        });
    });
  });
});