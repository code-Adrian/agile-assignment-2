import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import api from "../../../../index";
import User from "../../../../api/users/userModel";
import Review from "../../../../api/movies/movieReviewModel";

const expect = chai.expect;
let db;
let user1token;

describe("Movie reviews", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
    try{
    await Review.deleteMany();
    await User.deleteMany();
    await request(api).post("/api/users?action=register").send({username: "user1",password: "test1"});
    api.close();
    }catch(error){
        console.log(`Failed to create test user. Error: ${error}`)
    }
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  afterEach(() => {
    api.close(); // Release PORT 8080
  });

  describe("For receiving an authentication token", () => {
    it("should return a 200 status, a generated token and set global variable", () => {
      return request(api)
        .post("/api/users?action=authenticate")
        .send({username: "user1",password: "test1"})
        .expect(200)
        .then((res) => {
          expect(res.body.success).to.be.true;
          expect(res.body.token).to.not.be.undefined;
          user1token = res.body.token.substring(7);
        });
    });
});

describe("GET /api/movies/0/reviews", () => {
    it("should return status code of 401 (Unauthorized)", (done) => {
      request(api)
        .get("/api/movies/0/reviews")
        .expect(401)
        .end((err, res) => {
          expect(res.text).to.contain("Unauthorized")
          done();
        });
    });
  });


  describe("GET /api/movies/663712/reviews", () => {
    it("should return status 404 with no reviews for that movie.", (done) => {
        request(api)
          .get("/api/movies/663712/reviews")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .expect(404)
          .end((err, res) => {
            expect(res.text).to.contain("The resource you requested could not be found.");
            done();
          });
      });
  });

  describe("POST /api/movies/663712/reviews", () => {
    it("should create a new movie review and push the body.", () => {
        return request(api)
          .post("/api/movies/663712/reviews")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .send({id: 663712,
            results: [{
                "id": 0,
                "author": "Adrian",
                "content": "This is a movie review 1",
                "rating": "5"
            }]
        })
          .expect(201)
          .then((res) => {
            expect(res.text).to.contain("Created movie review and pushed review.");
          });
      });
  });

  describe("GET /api/movies/663712/reviews", () => {
    it("should find movie review and return 1 movie review.", (done) => {
        request(api)
          .get("/api/movies/663712/reviews")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .expect(201)
          .end((err, res) => {
            expect(res.body.results).to.be.a("array");
            expect(res.body.results.length).to.equal(1);
            done();
          });
      });
  });

  describe("POST /api/movies/663712/reviews", () => {
    it("should find existing movie review and push the body.", () => {
        return request(api)
          .post("/api/movies/663712/reviews")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .send({id: 663712,
            results: [{
                "id": 0,
                "author": "John",
                "content": "This is a movie review 2",
                "rating": "10"
            }]
        })
          .expect(201)
          .then((res) => {
            expect(res.text).to.contain("Found movie and pushed review.");
          });
      });
  });

  describe("GET /api/movies/663712/reviews", () => {
    it("should find movie review and return 2 movie review.", (done) => {
        request(api)
          .get("/api/movies/663712/reviews")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .expect(201)
          .end((err, res) => {
            expect(res.body.results).to.be.a("array");
            expect(res.body.results.length).to.equal(2);
            done();
          });
      });
  });

});