import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import api from "../../../../index";
import User from "../../../../api/users/userModel";
import Favourite from "../../../../api/movies/movieFavouriteModel";

const expect = chai.expect;
let db;
let user1token;
let user1 = "user1";

describe("Movie favourites", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
    try{
    await Favourite.deleteMany();
    await User.deleteMany();
    await request(api).post("/api/users?action=register").send({username: "user1",password: "test1"});
    api.close();
    }catch(error){
        console.log(`Failed to create test user. Error: ${error}`)
    }
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  afterEach(() => {
    api.close(); // Release PORT 8080
  });

  describe("For receiving an authentication token", () => {
    it("should return a 200 status, a generated token and set global variable", () => {
      return request(api)
        .post("/api/users?action=authenticate")
        .send({username: "user1",password: "test1"})
        .expect(200)
        .then((res) => {
          expect(res.body.success).to.be.true;
          expect(res.body.token).to.not.be.undefined;
          user1token = res.body.token.substring(7);
        });
    });
});

describe("GET /api/movies/user1/favourites", () => {
    it("should return status code of 401 (Unauthorized)", (done) => {
      request(api)
        .get("/api/movies/user1/favourites")
        .expect(401)
        .end((err, res) => {
          expect(res.text).to.contain("Unauthorized")
          done();
        });
    });
  });


  describe("GET /api/movies/user1/favourites", () => {
    it("should return status 404 with no favourites for that user", (done) => {
        request(api)
          .get("/api/movies/user1/favourites")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .expect(404)
          .end((err, res) => {
            expect(res.text).to.contain("The resource you requested could not be found.");
            done();
          });
      });
  });

  describe("POST /api/movies/user1/favourites", () => {
    it("should create a new  user favourite and add new favourite.", () => {
        return request(api)
          .post("/api/movies/user1/favourites")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .send({
            id: 0,
            username: user1,
            favourites: [{
                "movieId": "663712",
            }]
        })
          .expect(201)
          .then((res) => {
            expect(res.text).to.contain("Created user favourite collection and pushed favourite movie.");
          });
      });
  });

  describe("GET /api/movies/user1/favourites", () => {
    it("should find user and return status code of 201 with a favourite array of length 1.", (done) => {
        request(api)
          .get("/api/movies/user1/favourites")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .expect(201)
          .end((err, res) => {
            expect(res.body.favourites).to.be.a("array");
            expect(res.body.favourites.length).to.equal(1);
            done();
          });
      });
  });

  describe("POST /api/movies/user1/favourites", () => {
    it("should find user and update favourite movies.", () => {
        return request(api)
          .post("/api/movies/user1/favourites")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .send({
            id: 0,
            username: user1,
            favourites: [{
                "movieId": "663712",
            },
            {
                "movieId": "3",
            }]
        })
          .expect(201)
          .then((res) => {
            expect(res.text).to.contain("Found user and updated favourites.");
          });
      });
  });

  describe("GET /api/movies/user1/favourites", () => {
    it("should find user and return status code of 201 with a favourite array of length 2.", (done) => {
        request(api)
          .get("/api/movies/user1/favourites")
          .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
          .expect("Content-Type", /json/)
          .expect(201)
          .end((err, res) => {
            expect(res.body.favourites).to.be.a("array");
            expect(res.body.favourites.length).to.equal(2);
            done();
          });
      });
  });


});