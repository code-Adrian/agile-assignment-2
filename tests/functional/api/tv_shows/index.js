import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import api from "../../../../index";
import User from "../../../../api/users/userModel";

const expect = chai.expect;
let db;
let user1token;
let totalPages;
let tvShowName = "Pride";
let seededTvShowCreditsId = 119051; //SEEDED
let tvShowCreditsId = 197189; //TMDB (regular)


describe("Tv Show Enpoint", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
    try{
    await User.deleteMany();
    await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test1",
      });
      api.close();
    }catch(error){
        console.log(`Failed to create test user. Error: ${error}`)
    }
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  afterEach(() => {
    api.close(); // Release PORT 8080
  });


  describe("For receiving an authentication token", () => {
      it("should return a 200 status, a generated token and set global variable", () => {
        return request(api)
          .post("/api/users?action=authenticate")
          .send({
            username: "user1",
            password: "test1",
          })
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.token).to.not.be.undefined;
            user1token = res.body.token.substring(7);
          });
      });
  });

 
  describe("GET /api/tv/tmdb/tvShows/1 ", () => {
    it("should return status code of 401 (Unauthorized)", (done) => {
      request(api)
        .get("/api/tv/tmdb/tvShows/1")
        .expect(401)
        .end((err, res) => {
          expect(res.text).to.equal("Unauthorized")
          done();
        });
    });
  });

  describe("GET /api/tv/tmdb/tvShows/1 ", () => {
    it("should return 20 tv shows from page 1 and a status 200", (done) => {
      request(api)
        .get("/api/tv/tmdb/tvShows/1")
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          done();
        });
    });
  });

  describe("GET /api/tv/tmdb/tvShows/2 ", () => {
    it("should return 20 tv shows from page 2, set total pages variable expect a status code 200", (done) => {
      request(api)
        .get("/api/tv/tmdb/tvShows/2")
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          if(res.body.total_pages > 500){
            totalPages = 500
          }else{
            totalPages = res.body.total_pages
          }
          done();
        });
    });
  });

  describe("GET /api/tv/tmdb/tvShows/{last page} ", () => {
    it("should return more than 0 tv shows from last page and expect a status code 200", (done) => {
      request(api)
        .get(`/api/tv/tmdb/tvShows/${totalPages}`)
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.greaterThan(0);
          done();
        });
    });
  });

  describe("GET /api/tv/tmdb/tvShows/{invalid page} ", () => {
    it("should expect error 404 from requesting invalid page", (done) => {
      request(api)
        .get(`/api/tv/tmdb/tvShows/0`)
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(404)
        .end((err, res) => {
          expect(res.text).to.contain("Tv Shows not found for page:");
          done();
        });
    });
  });

  describe("GET /api/tv/tmdb/tvShow/1 ", () => {
    it("should return a tv show and a status 200", (done) => {
      request(api)
        .get("/api/tv/tmdb/tvShow/1")
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.genres).to.be.a("array");
          expect(res.body.id).to.equal(1);
          expect(res.body.name).to.equal(tvShowName);
          done();
        });
    });
  });

  describe("GET /api/tv/tmdb/tvShow/0", () => {
    it("should return 404 status code from an tv show invalid ID", (done) => {
      request(api)
        .get("/api/tv/tmdb/tvShow/0")
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(404)
        .end((err, res) => {
            expect(res.text).to.contain("Tv Show not found for id:");
          done();
        });
    });
  });

  describe("Tv Show Credits. (Seed Data || TMDB) ", () => {

    describe("GET /api/tv/tmdb/tvShow/credits/119051", () => {
        it("should return tv show credits from seeded data and a status 200", (done) => {
          request(api)
            .get(`/api/tv/tmdb/tvShow/credits/${seededTvShowCreditsId}`)
            .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
            .expect("Content-Type", /json/)
            .expect(200)
            .end((err, res) => {
              expect(res.body.id).to.equal(seededTvShowCreditsId);
              done();
            });
        });
      }); 

      describe("GET /api/tv/tmdb/tvShow/credits/197189", () => {
        it("should return tv show credits from tmdb and a status 200", (done) => {
          request(api)
            .get(`/api/tv/tmdb/tvShow/credits/${tvShowCreditsId}`)
            .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
            .expect("Content-Type", /json/)
            .expect(200)
            .end((err, res) => {
              expect(res.body.id).to.equal(197189);
              done();
            });
        });
      }); 
  });

  describe("GET /api/tv/tmdb/tvShow/credits/0", () => {
    it("should return 404 status code from an tv show credits invalid ID", (done) => {
      request(api)
        .get(`/api/tv/tmdb/tvShow/credits/0`)
        .set({'Accept': 'application/json', 'Authorization': `Bearer ${user1token}`})
        .expect("Content-Type", /json/)
        .expect(404)
        .end((err, res) => {
            expect(res.text).to.contain("No Credits were found for id:");
          done();
        });
    });
  }); 

});