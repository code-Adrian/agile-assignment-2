# Assignment 2 - Agile Software Practice.

Name: Adrian Bernacki

## API endpoints.

+ POST /api/movies/:id/reviews - Posts a review for a movie from an authenticated user / author. (Requires Authentication)
+ GET /api/movies/:id/reviews - Retrieves reviews for a specific movie. (Requires Authentication)
+ POST /api/movies/:user/favourites - Creates or updates favourite movies for a specific user. (Requires Authentication)
+ GET /api/movies/:user/favourites - Retrieves favourite movies for a specific user. (Requires Authentication)
+ GET /api/movies/tmdb/upcoming/:page - Retrieves a paginated json response of upcoming movies. (Requires Authentication)
+ GET /api/movies/tmdb/discover/:page - Retrieves a paginated json response of discover movies. (Requires Authentication)
+ GET /api/movies/tmdb/popular/:page - Retrieves a paginated json response of popular movies. (Requires Authentication)
+ GET /api/movies/tmdb/now_playing/:page - Retrieves a paginated json response of now playing movies. (Requires Authentication)
+ GET /api/users/ - Retrieves saved users from the database. (Requires Authentication)
+ POST /api/users/ (Auth or Register) - Authenticates or creates a user. (Doesn't Require Authentication) 
+ GET /api/tv/tmdb/tvShows/:page - Retrieves a paginated json response of tv shows. (Requires Authentication)
+ GET /api/tv/tmdb/tvShow/:id - Retrieves a tv show. (Requires Authentication)
+ GET /api/tv/tmdb/tvShow/credits/:id - retrieves tv show credits for a specific tv show. (Requires Authentication)


## Test cases.

~~~
 
  Movie favourites    
database connected to test on ac-m3ctxq3-shard-00-00.62khknp.mongodb.net
2 credits were successfully stored.
    For receiving an authentication token
      √ should return a 200 status, a generated token and set global variable (178ms)
    GET /api/movies/user1/favourites
      √ should return status code of 401 (Unauthorized)
    GET /api/movies/user1/favourites
      √ should return status 404 with no favourites for that user
    POST /api/movies/user1/favourites
Created user favourite collection and pushed favourite movie.
      √ should create a new  user favourite and add new favourite. (81ms)
    GET /api/movies/user1/favourites
      √ should find user and return status code of 201 with a favourite array of length 1.
    POST /api/movies/user1/favourites
Found user and updated favourites.
      √ should find user and update favourite movies. (48ms)
    GET /api/movies/user1/favourites
      √ should find user and return status code of 201 with a favourite array of length 2.

  Movie reviews
    For receiving an authentication token
      √ should return a 200 status, a generated token and set global variable (169ms)
    GET /api/movies/0/reviews
      √ should return status code of 401 (Unauthorized)
    GET /api/movies/663712/reviews
      √ should return status 404 with no reviews for that movie.
    POST /api/movies/663712/reviews
Created movie review and pushed review.
      √ should create a new movie review and push the body. (81ms)
    GET /api/movies/663712/reviews
      √ should find movie review and return 1 movie review. (46ms)
    POST /api/movies/663712/reviews
Found movie and pushed review.
      √ should find existing movie review and push the body. (52ms)
    GET /api/movies/663712/reviews
      √ should find movie review and return 2 movie review.

  Tv Show Enpoint
    For receiving an authentication token
      √ should return a 200 status, a generated token and set global variable (171ms)
    GET /api/tv/tmdb/tvShows/1 
      √ should return status code of 401 (Unauthorized)
    GET /api/tv/tmdb/tvShows/1
      √ should return 20 tv shows from page 1 and a status 200 (88ms)
    GET /api/tv/tmdb/tvShows/2 
      √ should return 20 tv shows from page 2, set total pages variable expect a status code 200 (82ms)
    GET /api/tv/tmdb/tvShows/{last page} 
      √ should return more than 0 tv shows from last page and expect a status code 200 (57ms)
    GET /api/tv/tmdb/tvShows/{invalid page} 
      √ should expect error 404 from requesting invalid page (125ms)
    GET /api/tv/tmdb/tvShow/1 
      √ should return a tv show and a status 200 (62ms)
    GET /api/tv/tmdb/tvShow/0
      √ should return 404 status code from an tv show invalid ID (129ms)
    Tv Show Credits. (Seed Data || TMDB) 
      GET /api/tv/tmdb/tvShow/credits/119051
Credits from local database could not be found, sending request towards TMDB...
TMDB credits acquired.
        √ should return tv show credits from seeded data and a status 200 (95ms)
      GET /api/tv/tmdb/tvShow/credits/197189
Credits from local database could not be found, sending request towards TMDB...
TMDB credits acquired.
        √ should return tv show credits from tmdb and a status 200 (63ms)
    GET /api/tv/tmdb/tvShow/credits/0
Credits from local database could not be found, sending request towards TMDB...
      √ should return 404 status code from an tv show credits invalid ID (147ms)

  Now Playing Movies
    For receiving an authentication token
      √ should return a 200 status, a generated token and set global variable (170ms)
    GET /api/movies/tmdb/now_playing/1 
      √ should return status code of 401 (Unauthorized)
    GET /api/movies/tmdb/now_playing/1
      √ should return 20 now_playing movies from page 1 and a status 200 (54ms)
    GET /api/movies/tmdb/now_playing/2 
      √ should return 20 now_playing movies from page 2, set total pages variable expect a status code 200 (58ms)
    GET /api/movies/tmdb/now_playing/{last page} 
      √ should return more than 0 now_playing movies from last page and expect a status code 200 (60ms)
    GET /api/movies/tmdb/now_playing/{invalid page} 
      √ should expect error 404 from requesting invalid page (127ms)

  Popular Movies
    For receiving an authentication token
      √ should return a 200 status, a generated token and set global variable (169ms)
    GET /api/movies/tmdb/popular/1 
      √ should return status code of 401 (Unauthorized)
    GET /api/movies/tmdb/popular/1
      √ should return 20 popular movies from page 1 and a status 200 (63ms)
    GET /api/movies/tmdb/popular/2 
      √ should return 20 popular movies from page 2, set total pages variable expect a status code 200 (50ms)
    GET /api/movies/tmdb/popular/{last page} 
      √ should return more than 0 popular movies from last page and expect a status code 200 (54ms)
    GET /api/movies/tmdb/popular/{invalid page} 
      √ should expect error 404 from requesting invalid page (127ms)

  Upcoming Movies
    For receiving an authentication token
      √ should return a 200 status, a generated token and set global variable (168ms)
    GET /api/movies/tmdb/upcoming/1 
      √ should return status code of 401 (Unauthorized)
    GET /api/movies/tmdb/upcoming/1
      √ should return 20 upcoming movies from page 1 and a status 200 (132ms)
    GET /api/movies/tmdb/upcoming/2 
      √ should return 20 upcoming movies from page 2, set total pages variable expect a status code 200 (148ms)
    GET /api/movies/tmdb/upcoming/{last page} 
      √ should return more than 0 upcoming movies from last page and expect a status code 200 (145ms)
    GET /api/movies/tmdb/upcoming/{invalid page} 
      √ should expect error 404 from requesting invalid page4 (119ms)

  Discover Movies
    For receiving an authentication token
      √ should return a 200 status, a generated token and set global variable (172ms)
    GET /api/movies/tmdb/discover/1 
      √ should return status code of 401 (Unauthorized)
    GET /api/movies/tmdb/discover/1
      √ should return 20 discover movies from page 1 and a status 200 (130ms)
    GET /api/movies/tmdb/discover/2 
      √ should return 20 discover movies from page 2, set total pages variable expect a status code 200 (215ms)
    GET /api/movies/tmdb/discover/{last page} 
      √ should return more than 0 discover movies from last page and expect a status code 200 (281ms)
    GET /api/movies/tmdb/discover/{invalid page}
      √ should expect error 404 from requesting invalid page (127ms)

  Users endpoint
    GET /api/users
      √ should return the 2 users and a status 200
    POST /api/users
      For a register action
        when the payload is correct
          √ should return a 201 status and the confirmation message (178ms)
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (168ms)
      invalid authentication. (username or password)
        √ should return a 401 status and a fail message


  53 passing (9s)

~~~

## Heroku

The express app is deployed to heroku [here](https://movies-api-staging-ab.herokuapp.com/api/movies/) 

## Independent Learning

I have managed to succesfuly deploy one endpoint (movies) into Vercel. I have decided to use this endpoint because it provides most functionality.
My Github repository for the Vercel deployment can be found [here](https://github.com/code-Adrian/agile-assignment-2-vercel) and the vercel URL [here](https://agile-assignment-2-vercel.vercel.app/)

Here are some images proving the vercel deployment works:


**Page 2 popular movies GET**
![An image of retrieving a JSON response from page 2 of popular movies](https://res.cloudinary.com/dv5ambux0/image/upload/v1672320828/get_page_2_popular_movies_b7fjef.png "Retrieving popular movies")

**Posting favourite movie for user1 POST**
![An image of posting a new movies favourite for user 1](https://res.cloudinary.com/dv5ambux0/image/upload/v1672320828/POST_movie_favourite_1_tprqhd.png "Posting movie favourite")

**Getting favourite movie for user1 GET**
![An image of getting movies favourite for user 1](https://res.cloudinary.com/dv5ambux0/image/upload/v1672320828/GET_movie_Favourite_1_iibj0j.png "Getting movie favourite")

**Posting / Updating favourite movie for user1 POST**
![An image of posting another movies favourite for user 1](https://res.cloudinary.com/dv5ambux0/image/upload/v1672320828/POST_movie_favourite_2_iei1ag.png "Posting movie favourite")

**Getting favourite movie for user1 GET**
![An image of getting movies favourites for user 1](https://res.cloudinary.com/dv5ambux0/image/upload/v1672320828/GET_movie_Favourite_2_vgxxc7.png "Getting movie favourites")